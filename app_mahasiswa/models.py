from django.db import models

# Create your models here.
class Skill(models.Model):
    skill_name = models.TextField()
    expertness = models.IntegerField()

class Mahasiswa(models.Model):
    npm = models.CharField('Student ID', max_length=20, primary_key=True)
    name = models.CharField('Name', max_length=200)
    email = models.TextField()
    description = models.TextField()
    linkedin_url = models.TextField()
    location = models.TextField(default="")
    picture_url = models.TextField(default="/static/img/Unknown.jpg")
    skills = models.ManyToManyField(Skill)
    show_score = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Status(models.Model):
    status = models.TextField(max_length=140)
    date = models.DateTimeField(auto_now_add=True)
    penulis = models.ForeignKey(Mahasiswa, related_name = 'stats')