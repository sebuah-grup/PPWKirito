from django.test import TestCase
from django.test import Client
from django.urls import resolve
import requests
import kirito.settings as settings

from .views_base import *
from .views_friends import *
from .views_login import *
from .views_profile import *
from .views_riwayat import *
from .views_status import *
from .models import Status
from kirito.custom_auth import *
from kirito.csui_login_helper import *

import json
import os
import environ
from importlib import import_module

# Create your tests here.
client_main = Client()

def setUpModule():
    print("\nTesting Part A: Mahasiswa")
    if 'django.contrib.sessions' in settings.INSTALLED_APPS:
        new_session = auth_login_for_test(client_main.session, "dummy")
        new_session.save()
    client_main.get('/mahasiswa/')


# Create your tests here.
class LoginUnitTest(TestCase):
    def test_login_url_is_exist(self):
        response = client_main.get('/mahasiswa/login/')
        self.assertEqual(response.status_code, 302)


    def test_fail_login(self):
        response = Client().get('/mahasiswa/login/')
        self.assertEqual(response.status_code, 200)

class DashboardUnitTest(TestCase):
    def test_dashboard_url_is_exist(self):
        response = client_main.get('/mahasiswa/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'mahasiswa-base.html')

    def test_root_url_now_is_using_index_page_from_dashboard_url(self):
        response = client_main.get('/')
        self.assertEqual(response.status_code, 301)
        self.assertRedirects(response,'/mahasiswa/',301,200)

    def test_fail_login(self):
        response = Client().get('/mahasiswa/')
        self.assertEqual(response.status_code, 200)

class FriendsUnitTest(TestCase):
    def test_login_url_is_exist(self):
        response = client_main.get(reverse('mahasiswa:friends'))
        self.assertEqual(response.status_code, 200)

    def test_user_login_exist_and_not(self):
        c = Client()
        session = c.session
        session['user_login'] = "someKey"
        session.save()
        c.get(reverse('mahasiswa:friends'))
        c = Client()
        response = c.get(reverse('mahasiswa:friends'))
        self.assertEqual(response.status_code, 200)

    def test_search_mahasiswa(self):
        c = Client()
        response = c.get(reverse('mahasiswa:search_user'), {'query' : '1606881531', 'attribute' : 'npm'})
        c.get(reverse('mahasiswa:search_user'), {'query' : '2016', 'attribute' : 'angkatan'})
        c.get(reverse('mahasiswa:search_user'), {'query' : 'affan ganteng', 'attribute' : 'nama'})
        c.get(reverse('mahasiswa:search_user'), {'query' : 'javascript', 'attribute' : 'keahlian'})
        c.get(reverse('mahasiswa:search_user'), {'query' : 'abcdefg', 'attribute' : 'level'}) #raise valueError
        c.get(reverse('mahasiswa:search_user'), {'query' : 'Expert', 'attribute' : 'level'}) #make sure it can also search level keyword
        c.get(reverse('mahasiswa:search_user'), {'query' : '5', 'attribute' : 'level'})
        c.get(reverse('mahasiswa:search_user'), {}) #Untuk di get_parameter_request
        self.assertEqual(response.status_code, 200)


class ProfileUnitTest(TestCase):
    def test_profile_page_is_exist(self):
        # if logged in
        response = client_main.get('/mahasiswa/profile/')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, "profile.html")

        # if not logged in
        response = Client().get('/mahasiswa/profile/')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, "login-mahasiswa.html")

    def test_edit_profile_page_is_exist(self):
        # if logged in
        response = client_main.get('/mahasiswa/profile/edit/')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, "profile-edit.html")

        # if not logged in
        response = Client().get('/mahasiswa/profile/edit/')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, "login-mahasiswa.html")

    def test_modify_profile(self):
        # testing basic profile modify
        toPost = {"formattedName":"Ade Kusuma", "location":'{"name":"Indonesia"}', "emailAddress":"kusuma@ade.co",
                    "publicProfileUrl":"http://project-kirito.herokuapp.com/", "summary":"Saya cupu.", "show_score":1}
        response = client_main.post('/mahasiswa/profile/save/', toPost)

        response = client_main.get('/mahasiswa/profile/edit/')
        html_response = response.content.decode('utf-8')
        self.assertIn("Ade Kusuma", html_response)
        self.assertIn("Indonesia", html_response)
        self.assertIn("kusuma@ade.co", html_response)
        self.assertIn("http://project-kirito.herokuapp.com/", html_response)
        self.assertIn("Saya cupu.", html_response)

        response = client_main.get('/mahasiswa/')
        html_response = response.content.decode('utf-8')
        self.assertIn("Saya cupu.", html_response)
        self.assertIn("Ade Kusuma", html_response)

        response = client_main.get('/mahasiswa/profile/')
        html_response = response.content.decode('utf-8')
        self.assertIn("Indonesia", html_response)
        self.assertIn("kusuma@ade.co", html_response)
        self.assertIn("http://project-kirito.herokuapp.com/", html_response)

        # testing add skill
        toPost = {"add_skills":'{"Java":5,"Python":3}', "change_skills":'{"0":{"name":"Angular","level":1}}'}
        response = client_main.post('/mahasiswa/profile/save/', toPost)

        response = client_main.get('/mahasiswa/profile/edit/')
        html_response = response.content.decode('utf-8')
        self.assertIn("Java", html_response)
        self.assertIn("Python", html_response)
        self.assertIn("Angular", html_response)

        response = client_main.get('/mahasiswa/profile/')
        html_response = response.content.decode('utf-8')
        self.assertIn("Java, Legend", html_response)
        self.assertIn("Python, Advanced", html_response)
        self.assertIn("Angular, Beginner", html_response)

        # testing remove skill
        toPost = {"remove_skills":'{"2":{"name":"Python","level":3}}',"change_skills":'{"1":{"name":"C++","level":4}}'}
        response = client_main.post('/mahasiswa/profile/save/', toPost)

        response = client_main.get('/mahasiswa/profile/edit/')
        html_response = response.content.decode('utf-8')
        self.assertIn("C++", html_response)
        self.assertIn("Java", html_response)

        response = client_main.get('/mahasiswa/profile/')
        html_response = response.content.decode('utf-8')
        self.assertIn("C++, Expert", html_response)
        self.assertIn("Java, Legend", html_response)

        # testing remove if it's not valid
        toPost = {"remove_skills":'{"5":{"name":"Saya","level":4}}'}
        response = client_main.post('/mahasiswa/profile/save/', toPost)
        self.assertEqual(response.status_code, 200)


class RiwayatUnitTest(TestCase):
    def test_riwayat_page_is_exist(self):
        # if logged in
        response = client_main.get('/mahasiswa/riwayat/')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, "riwayat.html")

        # if not logged in
        response = Client().get('/mahasiswa/riwayat/')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, "login-mahasiswa.html")

class StatusUnitTest(TestCase):
    def test_status_page_is_exist(self):
        response = client_main.get('/mahasiswa/status/')
        self.assertEqual(response.status_code,200)

    def test_main_page_redirects_to_status_page(self):
        response = client_main.get('/')
        self.assertEqual(response.status_code,301)

    def test_using_index_func(self):
        found = resolve('/mahasiswa/status/')
        self.assertEqual(found.func, status_index)

    def test_add_one_message_render(self):
        statusTest = 'test'
        postStatus = client_main.post('/mahasiswa/status/add_status/', {'status' : statusTest})

        self.assertEqual(postStatus.status_code, 302)

        responsePage = client_main.get('/mahasiswa/status/')
        html_response = responsePage.content.decode('utf8')
        self.assertIn(statusTest, html_response)

    def test_fail_login(self):
        response = Client().get('/mahasiswa/status/')
        self.assertEqual(response.status_code, 200)


    def test_remove_one_message(self):
        message = 'my name is jeff'
        mahasiswa = get_suitable_account(client_main)
        new_activity = Status.objects.create(status = message, penulis = mahasiswa)
        deleteJeff = client_main.post('/mahasiswa/status/delete_status/', {'status_id' : '1'})

        self.assertEqual(deleteJeff.status_code, 302)

        responsePage = deleteJeff.content.decode('utf8')
        self.assertNotIn(message, responsePage)
