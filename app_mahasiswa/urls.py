from django.conf.urls import url
from .views_base import *
from .views_friends import search_user, friends_index
from .views_login import *
from .views_profile import *
from .views_riwayat import *
from .views_status import *
from kirito.custom_auth import auth_login, auth_logout

#url for app
urlpatterns = [
    # base urls
    url(r'^$', index, name='index'),

    # login urls
    url(r'^login/$', login_index, name='login'),

    # profile urls
    url(r'^profile/$', profile_index, name='profile'),
    url(r'^profile/edit/$', edit_profile_index, name='edit-profile'),
    url(r'^profile/save/$', save_profile, name='save-profile'),

    # riwayat urls
    url(r'^riwayat/$', riwayat_index, name='riwayat'),

    # status urls
    url(r'^status/$', status_index, name='status'),
    url(r'^status/add_status/$', add_status, name='add_status'),
    url(r'^status/delete_status/$', delete_status, name='delete_status'),

    # friends urls
    url(r'^friends/$', friends_index, name='friends'),
    url(r'^search_user/$', search_user, name='search_user'),
    url(r'^auth_login/$', auth_login, name='auth_login'),
    url(r'^auth_logout/$', auth_logout, name='auth_logout')
]
