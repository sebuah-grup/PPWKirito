# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
from .models import Mahasiswa
from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
response = {'found_query' : None}

# Create your views here.
def friends_index(request):
    if 'user_login' in request.session:
        response['login'] = True
        return render(request, 'friends.html', response)
    else:
        response['login'] = False
        return render(request, 'login-mahasiswa.html', response)


#TODO kasih request and get di sini. Semua hasil dari pencarian tolong dijadikan json.
#TODO bila ditemukan, key found_query di respons jadiin true
#Template card ada di pingendo app
#level keahlian dari low ke high >>> biru, cyan, hijau, merah, golden
def search_user(request):
    query, attribute = get_parameter_request(request)
    user_list = []
    response['user_list'] = user_list

    print("Searching for ===> " + query + " in respect of its " + attribute)
    if attribute == 'npm':
        resultNPM = Mahasiswa.objects.filter(npm = query)
        user_list = [entry for entry in resultNPM]
        print("Found " + str(len(user_list)))

    if attribute == 'nama':
        resultName = Mahasiswa.objects.filter(name__icontains = query)
        user_list = [entry for entry in resultName]
        print("Found " + str(len(user_list)))

    if attribute == 'angkatan':
        if len(query) == 4:
            query = query[2:]
        resultAngkatan = Mahasiswa.objects.filter(npm__startswith=query)
        user_list = [entry for entry in resultAngkatan]
        print("Found " + str(len(user_list)))

    #Coloured differently based on its level
    if attribute == 'keahlian':
        resultKeahlian = Mahasiswa.objects.filter(skills__skill_name = query)
        user_list = [entry for entry in resultKeahlian]
        print("Found " + str(len(user_list)))

    #Expertness from 1(lowest) to 5(highest)
    if attribute == 'level':
        keywords = {"Beginner":1,"Intermediate":2,"Advanced":3,"Expert":4,"Legend":5}
        try: #bila query ternyata bukan angka
            if query in keywords: query_int = keywords[query]
            else: query_int = int(query)
            resultLevel = Mahasiswa.objects.filter(skills__expertness = query_int)
        except ValueError:
            resultLevel = {}
        user_list = [entry for entry in resultLevel]
        print("Found " + str(len(user_list)))

    response['user_list'] = user_list
    return render(request, 'friends.html', response)

#Search query and attribute that it searching with.
def get_parameter_request(request):
    if request.GET.get("query"):
        query = request.GET.get("query")
    else:
        query = "-"

    if request.GET.get("attribute"):
        attribute = request.GET.get("attribute")
    else:
        attribute = "-"

    return query, attribute