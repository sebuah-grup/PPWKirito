# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
response = {}

# Create your views here.
def login_index(request):
    if 'user_login' in request.session:
        set_data_for_session(response, request)
        return HttpResponseRedirect(reverse('mahasiswa:index'))
    else:
        response['login'] = False
        return render(request, 'login-mahasiswa.html', response)
def set_data_for_session(resp, request):
    resp['user_login'] = request.session['user_login']
    resp['nama'] = request.session['user_login']
    resp['npm'] = request.session['kode_identitas']
    resp['access_token'] = request.session['access_token']
    resp['kode_identitas'] = request.session['kode_identitas']
    resp['role'] = request.session['role']
    resp['login'] = True
