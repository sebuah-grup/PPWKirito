# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ObjectDoesNotExist

from .models import Mahasiswa, Skill
from .views_login import set_data_for_session
response = {}

# Create your views here.
def profile_index(request):
    if 'user_login' in request.session:
        response['login'] = True
        set_data_for_session(response, request)
        response['mahasiswa'] = get_suitable_account(request)
        response['mahasiswa_skills'] = response['mahasiswa'].skills.all()
        return render(request, 'profile.html', response)
    else:
        response['login'] = False
        return render(request, 'login-mahasiswa.html', response)

def get_suitable_account(request):
    npm = request.session.get('kode_identitas')
    try:
        print("searching for existing user")
        mahasiswa = Mahasiswa.objects.get(npm = npm)
    except Exception as e:
        print("creating a new account SSO")
        mahasiswa = create_new_account(request)
    return mahasiswa

def create_new_account(request):
    name = request.session.get('user_login')
    npm = request.session.get('kode_identitas')

    mahasiswa = Mahasiswa()
    mahasiswa.name = name
    mahasiswa.npm = npm
    mahasiswa.email = ""
    mahasiswa.description = ""
    mahasiswa.linkedin_url = ""
    mahasiswa.save()
    return mahasiswa

def edit_profile_index(request):
    if 'user_login' in request.session:
        response['login'] = True
        set_data_for_session(response, request)
        response['mahasiswa'] = get_suitable_account(request)
        response['mahasiswa_skills'] = response['mahasiswa'].skills.all()
        print(response['mahasiswa_skills'])
        return render(request, 'profile-edit.html', response)
    else:
        response['login'] = False
        return render(request, 'login-mahasiswa.html', response)

def get_skill(name, value, lazy = False):
    try:
        skill = Skill.objects.get(skill_name = name, expertness = value)
    except ObjectDoesNotExist:
        if not lazy:
            skill = Skill(skill_name = name, expertness = value)
            skill.save()
        else: skill = None
    return skill

def save_profile(request):
    npm = request.session.get("kode_identitas")
    mahasiswa = Mahasiswa.objects.get(npm = npm)

    mahasiswa.name = request.POST.get("formattedName",mahasiswa.name)
    mahasiswa.email = request.POST.get("emailAddress",mahasiswa.email)
    mahasiswa.location = json.loads(request.POST.get("location","{}")).get("name",mahasiswa.location)
    mahasiswa.linkedin_url = request.POST.get("publicProfileUrl",mahasiswa.linkedin_url)
    mahasiswa.picture_url = request.POST.get("pictureUrl",mahasiswa.picture_url)
    mahasiswa.description = request.POST.get("summary",mahasiswa.description)
    print(request.POST.get("show_score",0))
    mahasiswa.show_score = True if int(request.POST.get("show_score",0)) > 0 else False

    add_skills = json.loads(request.POST.get("add_skills","{}"))
    change_skills = json.loads(request.POST.get("change_skills","{}"))
    remove_skills = json.loads(request.POST.get("remove_skills","{}"))

    print(add_skills)
    print(change_skills)
    print(remove_skills)

    for x in remove_skills:
        skill = get_skill(remove_skills[x]['name'], remove_skills[x]['level'], lazy = True)
        if skill != None: mahasiswa.skills.remove(skill)

    for x in change_skills:
        try:
            skill = Skill.objects.get(id = x)
        except:
            skill = None
        if skill != None and skill.skill_name != x and skill.expertness != change_skills[x]:
            mahasiswa.skills.remove(skill)

        skill = get_skill(change_skills[x]['name'], change_skills[x]['level'], lazy = False)
        mahasiswa.skills.add(skill)


    for x in add_skills:
        skill = get_skill(x, add_skills[x], lazy = False)
        mahasiswa.skills.add(skill)

    mahasiswa.save()
    print(mahasiswa.name)
    print(mahasiswa.location)
    return JsonResponse({"success":"true"})
