# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from .views_login import set_data_for_session
from .views_profile import get_suitable_account

response = {}

# Create your views here.
def riwayat_index(request):
    if 'user_login' in request.session:
        response['login'] = True
        set_data_for_session(response, request)
        response["mahasiswa"] = get_suitable_account(request)		
        return render(request, 'riwayat.html', response)
    else:
        response['login'] = False
        return render(request, 'login-mahasiswa.html', response)
        