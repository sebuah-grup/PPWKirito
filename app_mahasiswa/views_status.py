# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from .forms import Add_Status_Form

from .models import Status
from .views_login import set_data_for_session
from .views_profile import get_suitable_account
response = {}

# Create your views here.
def status_index(request):
    response = {'status' : Status.objects.all(), 'add_status_form' : Add_Status_Form}
    if 'user_login' in request.session:
        response['login'] = True
        set_data_for_session(response, request)
    else:
        response['login'] = False
    return render(request, 'status.html', response)

def add_status(request):
    form = Add_Status_Form(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        mahasiswa = get_suitable_account(request)
        status_now = Status(status=request.POST['status'], penulis = mahasiswa)
        response['status'] = request.POST['status']
        status_now.save()
    return HttpResponseRedirect(reverse('mahasiswa:status'))

def delete_status(request):
    if request.method == 'POST':
        inventory = Status.objects.last()
        status_id = int(request.POST['status_id'])
        status_to_delete = Status.objects.get(id=status_id)
        status_to_delete.delete()
        return HttpResponseRedirect(reverse('mahasiswa:status'))