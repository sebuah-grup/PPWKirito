from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse

from .csui_login_helper import get_access_token, verify_user

#authentication
def auth_login(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        #call csui_helper
        access_token = get_access_token(username, password)
        if access_token is not None:
            ver_user = verify_user(access_token)
            kode_identitas = ver_user['identity_number']
            role = ver_user['role']

            # set session
            request.session['user_login'] = username
            request.session['access_token'] = access_token
            request.session['kode_identitas'] = kode_identitas
            request.session['role'] = role
            messages.success(request, "Login success.")
        else:
            messages.warning(request, "Wrong username or password.")
    return HttpResponseRedirect(request.META.get('HTTP_REFERER','/'))

def auth_logout(request):
    request.session.flush() # menghapus semua session
    messages.warning(request, "Logout successful. Your session has been removed.")
    return HttpResponseRedirect(request.META.get('HTTP_REFERER','/'))

# manipulate login for testing purposes, by adding things on session
def auth_login_for_test(request_session, username):
    request_session['user_login'] = username
    request_session['access_token'] = 'xxxxxxxxxxxxxxxxxxxxxxx'
    request_session['kode_identitas'] = '0000000000'
    request_session['role'] = 'mahasiswa'

    return request_session